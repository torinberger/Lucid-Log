
const router = require('koa-joi-router');
const Joi = router.Joi;

const api = router();
      api . prefix('/api');
      api . use(require('./auth')) // load in auth routes
      api . use(require('./settings')) // load in settings routes
      api . use(require('./log')) // load in settings routes

module.exports = api.middleware()
