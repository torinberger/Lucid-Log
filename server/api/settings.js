
const database = require('../database'); // load in database controllers
const User = database.user;
const router = require('koa-joi-router');
const Joi = router.Joi;

const settings = router(); // create router
      settings . prefix('/settings');

settings.route({ // login auth route
  method: 'post',
  path: '/setup',
  validate: {
    body: {
      username: Joi.string().max(100),
      password: Joi.string().max(100),
      logDay: Joi.boolean(),
      journalNight: Joi.boolean(),
      logWBTBs: Joi.boolean(),
      logDreamTags: Joi.boolean(),
      logTechniques: Joi.boolean(),
    },
    type: 'json'
  },
  handler: async (ctx) => {
    let targetUser = await User.findOne({
      username: ctx.request.body.username,
      password: ctx.request.body.password
    }) // get user from DB

    targetUser.logDay = ctx.request.body.logDay
    targetUser.journalNight = ctx.request.body.journalNight
    targetUser.logWBTBs = ctx.request.body.logWBTBs
    targetUser.logDreamTags = ctx.request.body.logDreamTags
    targetUser.logTechniques = ctx.request.body.logTechniques

    let updatedUser = await targetUser.save().catch((err) => {
      ctx.status = 500
    })

    if (targetUser) { // if user updated
      ctx.body = targetUser
      ctx.status = 200
    } else { // if unkown error
      ctx.status = 500
    }
  }
})

settings.route({ // register auth route
  method: 'post',
  path: '/update',
  validate: {
    body: {
      username: Joi.string().max(100),
      password: Joi.string().max(100)
    },
    type: 'json'
  },
  handler: async (ctx) => {
    let targetUser = await new User(ctx.request.body).save().catch((err) => { // create new user, if error catch it
      let errCode = err.code; // get mongo error code

      if (errCode == 11000) { // if error code mean username taken
        ctx.status = 401
      } else { // if unkown error
        ctx.status = 500
      }
    })

    if (targetUser) { // if user created
      ctx.body = targetUser
      ctx.status = 201
    } else { // if unkown error
      ctx.status = 401
    }
  }
})

module.exports = settings.middleware()
