
const database = require('../database'); // load in database controllers
const User = database.user;
const Date = database.date;
const Day = database.day;
const Night = database.night;
const Dream = database.dream;
const Wbtb = database.wbtb;
const router = require('koa-joi-router');
const Joi = router.Joi;

const log = router(); // create router
      log . prefix('/log');

async function findOneOrCreate(model, condition, callback) {
  model.findOne(condition, (err, result) => {
    return result ? callback(err, result) : model.create(condition, (err, result) => { return callback(err, result) })
  })
}

log.route({ // login auth route
  method: 'post',
  path: '/update',
  validate: {
    type: 'json'
  },
  handler: async (ctx) => {
    console.log(ctx.request.body);
    let postData = ctx.request.body
    let targetUser = await User.findOne({_id: postData.user}) // get user from DB

    if (targetUser) { // if user exists

      let targetDate = await Date
        .findOneAndUpdate({
          user: postData.user,
          date: postData.date
        }, {}, {
          upsert: true,
          new: true
        })

      console.log(targetDate);

      let targetDay = await Day
        .findOneAndUpdate({
          date: targetDate._id
        }, {
          $set: postData.day
        }, {
          upsert: true,
          new: true
        })

      console.log(targetDay);

      let nightData = {
        techniques: postData.night.techniques,
        plans: postData.night.plans,
        outcomes: postData.night.outcomes,
        timeSlept: postData.night.timeSlept,
      }

      let dreams = postData.night.dreams
      let wbtbs = postData.night.wbtbs

      let targetNight = await Night
        .findOneAndUpdate({
          date: targetDate._id
        }, {
          $set: nightData
        }, {
          upsert: true,
          new: true
        })

      console.log(targetNight);

      await Dream.find({night: targetNight._id}).remove()

      for (let i = 0; i < dreams.length; i++) {
        let targetDream = await Dream
          .findOneAndUpdate({
            night: targetNight._id
          }, {
            $set: dreams[i]
          }, {
            upsert: true,
            new: true
          })

        console.log(targetDream);
      }

      await Wbtb.find({night: targetNight._id}).remove()

      for (let i = 0; i < wbtbs.length; i++) {
        let targetWbtb = await Wbtb
          .findOneAndUpdate({
            night: targetNight._id
          }, {
            $set: wbtbs[i]
          }, {
            upsert: true,
            new: true
          })

        console.log(targetWbtb);
      }

      ctx.status = 200

    } else { // if incorrect username/password
      ctx.status = 401
    }
  }
})

log.route({ // login auth route
  method: 'post',
  path: '/get',
  validate: {
    type: 'json'
  },
  handler: async (ctx) => {
    console.log(ctx.request.body);
    let postData = ctx.request.body
    let targetUser = await User.findOne({_id: postData.user}) // get user from DB

    if (targetUser) { // if user exists

      let targetDate = await Date
        .findOne({
          user: postData.user,
          date: postData.date
        })

      console.log(targetDate);

      if(targetDate) {

        let targetDay = await Day
          .findOne({
            date: targetDate._id
          })

        console.log(targetDay);

        let targetNight = await Night
          .findOne({
            date: targetDate._id
          })

        console.log(targetNight);

        let targetDreams = await Dream
          .find({
            night: targetNight._id
          })

        console.log(targetDreams);

        let targetWbtbs = await Wbtb
          .find({
            night: targetNight._id
          })

        console.log(targetWbtbs);

        ctx.body = {
          day: {
            plans: targetDay.plans || '',
            outcomes: targetDay.outcomes || '',
            RCs: targetDay.RCs || 0
          },
          night: {
            timeSlept: targetNight.timeSlept || '',
            plans: targetNight.plans || '',
            outcomes: targetNight.outcomes || '',
            techniques: targetNight.techniques || [],
            dreams: targetDreams,
            wbtbs: targetWbtbs
          }
        }

        ctx.status = 200

      } else {
        ctx.body = {
          day: {
            plans: '',
            outcomes: '',
            RCs: 0
          },
          night: {
            timeSlept: '',
            plans: '',
            outcomes: '',
            techniques: [],
            dreams: [],
            wbtbs: []
          }
        }

        ctx.status = 200
      }

    } else { // if incorrect username/password
      ctx.status = 401
    }
  }
})

module.exports = log.middleware()
