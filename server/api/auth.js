
const database = require('../database'); // load in database controllers
const User = database.user;
const router = require('koa-joi-router');
const Joi = router.Joi;

const auth = router(); // create router
      auth . prefix('/auth');

auth.route({ // login auth route
  method: 'post',
  path: '/login',
  validate: {
    body: {
      username: Joi.string().max(100),
      password: Joi.string().max(100)
    },
    type: 'json'
  },
  handler: async (ctx) => {
    let targetUser = await User.findOne(ctx.request.body) // get user from DB

    if (targetUser) { // if user exists
      ctx.body = targetUser
      ctx.status = 200
    } else { // if incorrect username/password
      ctx.status = 401
    }
  }
})

auth.route({ // register auth route
  method: 'post',
  path: '/register',
  validate: {
    body: {
      username: Joi.string().max(100),
      password: Joi.string().max(100),
      startDate: Joi.string().max(100)
    },
    type: 'json'
  },
  handler: async (ctx) => {
    let targetUser = await new User(ctx.request.body).save().catch((err) => { // create new user, if error catch it
      let errCode = err.code; // get mongo error code

      if (errCode == 11000) { // if error code mean username taken
        ctx.status = 401
      } else { // if unkown error
        ctx.status = 500
      }
    })

    if (targetUser) { // if user created
      ctx.body = targetUser
      ctx.status = 201
    } else { // if unkown error
      ctx.status = 401
    }
  }
})

module.exports = auth.middleware()
