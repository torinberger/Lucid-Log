
require('dotenv').config(); // load in .env variables

const koa = require('koa');
const cors = require('@koa/cors'); // for cross origin requests
const router = require('koa-joi-router');

const server = router();
      server . use(require('./utils/requestLog.js')) // global logger
      server . use(require('./api')) // load in api routes

const app = new koa()
      app . use(cors()) // for cross origin requests
      app . use(server.middleware()) // load in all server routes
      app . listen(process.env.PORT) // listen on port
