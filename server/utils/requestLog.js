

module.exports = async (ctx, next) => {
  await next(); // wait until routing completed
  console.log(`[${ctx.request.method}] ${ctx.request.path}`); // log path and method
  console.log(ctx.request.body || null); // log post data
}
