
let text = `I was at work (at the backing). I was in my dragon form and I was wearing a Starfleet uniform. I was talking with someone (who was also wearing a Starfleet uniform) about my knowledge on mythology and stuff. (or perhaps that someone else was talking about their knowledge about it. ... Or perhaps it was actually Dora the Explorer talking, I kunda forgot tbh.) Then the subject was changed to the Cthulhu Mythos.

Then, out of nowhere a weird alien thing suddenly shows up. It was the size of a human and it kinda looked like Cthulhu, but it was wearing a helmet and armour and it was welding some kind of large ray gun. It was a hostile alien. I tried to fight it but I didn't stand a chance against it. Before I could get a hit in, I was slapped by it and was sent flying to the other side of the small warehouse that is right next to the backing. There I hit the wall/backing. I slumped to the ground and was immobilised and only semi-conscious.

The creature then walked towards me. But a colleague in a forklift showed up. (He was also wearing a Starfleet uniform). The colleague on the forklift had a phaser with him. He hid himself and the forklift behind a warehouse rack, and waited for the creature to get close enough to him to ambush it. (That warehouse rack was sitting in the wrong place tho).

The ambush was a success. And the colleague shot the alien creature down. But then a second alien creature showed up. (This one had a different weapon than the first one.) The second creature then shot my colleague on the forklift. Somehow that colleague ended up lying semi-conscious next to me. The creatures weren't using the kill setting on their weapons.
Then the first creature showed up again. Apperantly being shot with a phaser didn't do it much harm. It saw that we were still somewhat concious. It then shot my colleague again, and then it shot me. Just before that moment though, I remembered thing to myself: "I wonder where they'll take us, hopefully not to Cthulhu himself, or we will go mad!".

Then I woke up`


let structures = [`a`, `about`,  `above`,  `after`,  `after`,  `again`,  `against`,  `ago`,  `ahead`,  `all`,  `almost`,  `almost`,  `along`,  `already`,  `also`,  `although`,  `always`,  `am`,  `among`,  `an`, `and`,  `any`,  `are`,   `aren't`,  `around`,  `as`,  `at`,  `away`,
`backward`,  `backwards`,  `be`,  `because`,  `before`,  `behind`,  `below`,  `beneath`,  `beside`,  `between`,  `both`,  `but`,  `by`,
`can`,  `cannot`,  `can't`,  `cause`,  `cos`,  `could`,  `couldn't`,
`'d`,  `despite`,  `did`,  `didn't`,  `do`,  `does`,  `doesn't`,  `don't`,  `down`,  `during`,
`each`,  `either`,  `even`,  `ever`,  `every`,  `except`,
`for`, `forward`,  `from`,
`had`, `hadn't`,  `has`,  `hasn't`,  `have`, `haven't`,  `he`, `her`, `here` , `hers`,  `herself`,  `him`,  `himself`,  `his`,  `how`,  `however`,  `I`,
`if`,  `in`,  `inside`,  `inspite`,  `instead`,  `into`,  `is`,  `isn't`,  `it`,  `its`,  `itself`,
`just`,
`'ll`,  `least`,  `less`,  `like`,
`'m`, `many`,  `may`,  `mayn't`,  `me`,  `might`,  `mightn't`, `mine`,  `more`,  `most`,  `much`,  `must`,  `mustn't`,  `my`,  `myself`,
`near`,  `need`,  `needn't`,  `needs`,  `neither`,  `never`,  `no`,  `none`,  `nor`,  `not`,  `now`,
`of`,  `off`,  `often`,  `on`,  `once`,  `only`,  `onto`,  `or`,  `ought`,  `oughtn't`,  `our`,  `ours`,  `ourselves`,  `out`,  `outside`,  `over` ,
`past`,  `perhaps` ,
`quite`,
`'re`, `rather`,
`'s`,  `seldom`,  `several`,  `shall`,  `shan't`,  `she`,  `should`, `shouldn't`, `since`,  `so`,  `some`,  `sometimes`,  `soon` ,
`than`,  `that`,  `the`,  `their`,  `theirs`,  `them`,  `themselves`,  `then`,  `there`,  `therefore`,  `these`,  `they`,  `this`,  `those`,  `though`,  `through`,  `thus`,  `till`,  `to`, `together`,  `too`,  `towards`,
`under`,  `unless`,  `until`,  `up`,  `upon`,  `us`,  `used`,  `usedn't`, `usen't`,  `usually`,
`'ve`, `very`,
`was`,  `wasn't`,  `we`,  `well`,  `were`,  `weren't`,  `what`,  `when`,  `where`,  `whether`,  `which`,  `while`,  `who`,  `whom`,  `whose`,  `why`,  `will`,  `with`,  `without`,  `won't`,  `would`,  `wouldn't`,
`yet`,  `you`,  `your`,  `yours`,  `yourself`,  `yourselves`]


for (var i = 0; i < structures.length; i++) {
	structures[i] = structures[i].replace(new RegExp("'", 'g'), '')
	structures[i] = structures[i].toLowerCase()
}

text = text
				.toLowerCase()
				.replace(new RegExp('\'', 'g'), '')
				.split('.').join('')
				.split('!').join('')
				.split('(').join('')
				.split(')').join('')
				.split(',').join('')
				.split(':').join('')
				.split('"').join('')
				.split('\n').join(' ')

console.log(text);

let words = text.split(' ')

for (var i = 0; i < words.length; i++) {
	if(structures.includes(words[i])) {
		words.splice(i, 1)
		i = 0
	}
}

console.log('new');
console.log(words.join(' '));

let countsWords = []
let countsN = []

for (let i = 0; i < words.length; i++) {
	if(countsWords.includes(words[i])) {
		countsN[countsWords.indexOf(words[i])] += 1
	} else {
		countsWords.push(words[i])
		countsN.push(1)
	}
}

let complete = []

for (let i = 0; i < countsWords.length; i++) {
	if (countsWords[i].length > 2) {
		complete.push({
			word: countsWords[i],
			count: countsN[i]
		})
	}
}

console.log(complete.sort((a, b) => {
	if(a.count > b.count) {
		return -1
	} else if(b.count > a.count) {
		return 1
	} else {
		return 0
	}
}));
