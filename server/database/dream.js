
const mongoose = require('mongoose')
const Schema = mongoose.Schema

// Create schema
const DreamSchema = new Schema({
  night: { type: Schema.Types.ObjectId, ref: 'Night' },
  clarity: { type: Number },
  length: { type: Number },
  lucidity: { type: Number },
  description: { type: String },
  tags: [{ type: String }]
})

// Create model
const Dream = mongoose.model('Dream', DreamSchema)

// Export to use in other files
module.exports = Dream;
