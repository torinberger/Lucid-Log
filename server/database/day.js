
const mongoose = require('mongoose')
const Schema = mongoose.Schema

// Create schema
const DaySchema = new Schema({
  date: { type: Schema.Types.ObjectId, ref: 'Date' },
  plans: { type: String },
  outcomes: { type: String },
  RCs: { type: Number }
})

// Create model
const Day = mongoose.model('Day', DaySchema)

// Export to use in other files
module.exports = Day;
