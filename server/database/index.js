
const mongoose = require('mongoose')

mongoose.Promise = global.Promise

// MLAB Database
mongoose.connect(`mongodb://${process.env.DBUSER}:${process.env.DBPASS}@ds123783.mlab.com:23783/lucid-log-new`, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false
})

// Connect to the Database
mongoose.connection.once('open', function () {
  console.log('[database] Connected to MongoDB')
}).on('error', function (error) {
  console.log('[database] Connection error', error)
})

module.exports = { // return database controllers
  user: require('./user'),
  date: require('./date'),
  day: require('./day'),
  dream: require('./dream'),
  night: require('./night'),
  wbtb: require('./wbtb')
}
