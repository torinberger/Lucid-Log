
const mongoose = require('mongoose')
const Schema = mongoose.Schema

// Create schema
const NightSchema = new Schema({
  date: { type: Schema.Types.ObjectId, ref: 'Date' },
  techniques: [{ type: String }],
  plans: { type: String },
  outcomes: { type: String },
  timeSlept: { type: String }
})

// Create model
const Night = mongoose.model('Night', NightSchema)

// Export to use in other files
module.exports = Night;
