
const mongoose = require('mongoose')
const Schema = mongoose.Schema

// Create schema
const WBTBSchema = new Schema({
  night: { type: Schema.Types.ObjectId, ref: 'Night' },
  techniques: [{ type: String }],
  time: { type: String },
  description: { type: String },
  length: { type: String }
})

// Create model
const WBTB = mongoose.model('WBTB', WBTBSchema)

// Export to use in other files
module.exports = WBTB;
