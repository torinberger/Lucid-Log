
const mongoose = require('mongoose')
const Schema = mongoose.Schema

// Create schema
const UserSchema = new Schema({
  username: { type: String, required: true, index: true, unique: true },
  password: { type: String, required: true },
  logDay: { type: Boolean, default: true },
  journalNight: { type: Boolean, default: true },
  logWBTBs: { type: Boolean, default: true },
  logDreamTags: { type: Boolean, default: true },
  logTechniques: { type: Boolean, default: true },
  startDate: { type: String, required: true }
})

// Create model
const User = mongoose.model('User', UserSchema)

// Export to use in other files
module.exports = User;
