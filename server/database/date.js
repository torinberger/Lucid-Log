
const mongoose = require('mongoose')
const Schema = mongoose.Schema

// Create schema
const DateSchema = new Schema({
  user: { type: Schema.Types.ObjectId, ref: 'User' },
  date: { type: String }
})

// Create model
const Date = mongoose.model('Date', DateSchema)

// Export to use in other files
module.exports = Date;
