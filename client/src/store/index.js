import Vue from 'vue'
import Vuex from 'vuex'

// import example from './module-example'

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      // example
    },

    state: {
      authentication: {},
      preferences: {}
    },

    mutations: {
      setAuth(state, authData) { // used to set authentication details
        state.authentication = authData
      },
      revokeAuth(state) { // used to revoke authentication details
        state.authentication = {}
        state.preferences = {}
      },
      setPreferences(state, preferences) {
        state.preferences = preferences
      }
    },

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEV
  })

  return Store
}
