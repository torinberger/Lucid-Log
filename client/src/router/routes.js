
const routes = [
  {
    path: '/',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: '/auth', component: () => import('pages/Auth.vue') },
      { path: '/setup', component: () => import('pages/Setup.vue') },
      { path: '/settings', component: () => import('pages/Settings.vue') },
      { path: '/log', component: () => import('pages/Log.vue') },
      { path: '/stats', component: () => import('pages/Stats.vue') },
      { path: '/profile', component: () => import('pages/Profile.vue') },
      { path: '/leaderboard', component: () => import('pages/Leaderboard.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
